<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api;

class Repositories extends Api
{
	/**
	 * @var \bitbucket\api\repositories\Changesets
	 */
	public $changesets;

	/**
	 * @var \bitbucket\api\repositories\DeployKeys
	 */
	public $deploy_keys;

	/**
	 * @var \bitbucket\api\repositories\Events
	 */
	public $events;

	/**
	 * @var \bitbucket\api\repositories\Followers
	 */
	public $followers;

	/**
	 * @var \bitbucket\api\repositories\Issues
	 */
	public $issues;

	/**
	 * @var \bitbucket\api\repositories\Links
	 */
	public $links;

	/**
	 * @var \bitbucket\api\repositories\Pullrequests
	 */
	public $pullrequests;

	/**
	 * @var \bitbucket\api\repositories\RepoSlug
	 */
	public $repo_slug;

	/**
	 * @var \bitbucket\api\repositories\Services
	 */
	public $services;

	/**
	 * @var \bitbucket\api\repositories\Src
	 */
	public $src;

	/**
	 * @var \bitbucket\api\repositories\Wiki
	 */
	public $wiki;

	/**
	 * Extended Constructor, forces a pass-thru context
	 */
	public function __construct(Api $api)
	{
		parent::__construct(
				$api->getOption("username"),
				$api->getOption("password"),
				$api->getOption("format", Api_format::OBJECT),
				$api->debug
		);

	}

	/**
	 * Assign all APIs to their respective chain-segment
	 */
	public function assign_apis()
	{
		$refclass = new \ReflectionClass( $this );
		foreach( $refclass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property )
		{
			$name = $property->name;
			if ( $property->class == $refclass->name )
			{
				$by_ref = explode("\\", __CLASS__);
				$this->{$property->name} = $this->getApi(end($by_ref), $property->name);
			}
		}
	}
}