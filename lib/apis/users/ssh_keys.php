<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\users;

use bitbucket\api\Api;
use bitbucket\api\ApiBase;


/**
 * Use the ssh-keys resource to manipulate the ssh-keys on an individual or team account.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class SshKeys extends ApiBase
{

    /**
     * Gets a list of the keys associated with an account. This call requires authentication.
     * @param string $account_name	The name of an individual or team account.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function show( $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $response = $this->api->get( "/users/{$account_name}/ssh-keys" );

        return $response;
    }

    /**
     * Creates a key on the specified account. You must supply a valid key that is unique across the Bitbucket service.
     * @param string $label			The user-visible label on the key.
     * @param string $key			Public key value
     * @param string $account_name	The name of an individual or team account.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function create( $label, $key, $account_name = null )
    {
        $response = null;
        $data = array();

        $this->checkUsername( $account_name );
        $data = array(
            'key' => $key, 'label' => $label
        );

        $response = $this->api->post( "/users/{$account_name}/ssh-keys", $data );

        return $response;
    }

    /**
     * Updates a key on the specified account. You must supply a valid key_id (pk field) that is unique across the Bitbucket service. You cannot update the key_id itself.
     * @param int		$key_id			The key identifier (ID).
     * @param string	$label			The user-visible label on the key.
     * @param string	$key			Public key value
     * @param string	$account_name	The name of an individual or team account
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function update( $key_id, $label, $key, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );
        $data = array(
            'key' => $key, 'label' => $label
        );
        $data = array_filter( $data );

        $response = $this->api->put( "/users/{$account_name}/ssh-keys/{$key_id}", $data );

        return $response;
    }

    /**
     * Gets the content of the specified key_id. This call requires authentication.
     * @param int		$key_id			The key identifier (ID).
     * @param string	$account_name	The name of an individual or team account
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function show_by_key( $key_id, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $response = $this->api->get( "/users/{$account_name}/ssh-keys/{$key_id}" );

        return $response;
    }

    /**
     * Deletes the key specified by the key_id value. This call requires authentication.
     * @param int		$key_id			The key identifier (ID).
     * @param string	$account_name	The name of an individual or team account
     * @return boolean
     */
    public function delete( $key_id, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $this->api->delete( "/users/{$account_name}/ssh-keys/{$key_id}" );
        $response = $this->api->getRequest()->http_code == '204' ? true : false;

        return $response;
    }
}