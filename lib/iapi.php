<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 */
namespace bitbucket\api;

/**
 * Just an Interface
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 * @package Bitbucket Api Library
 * @subpackage Core
 */
interface iApi
{

}