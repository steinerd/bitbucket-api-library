<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\repositories;

use \bitbucket\api\Helper;
use \bitbucket\api\Api;
use \bitbucket\api\ApiBase;

/**
 * Manage ssh keys used for deploying product builds. All the calls for this resource require authentication as the account owner.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Followers extends ApiBase
{
	/**
	 * Gets a count and the list of accounts following a repository. Use this API to get a list of the accounts following a repository.
	 * @param string $repo_slug The repository identifier.
	 * @param string $account_name The team or individual account owning the repository.
	 * @return \bitbucket\api\Ambigous
	 */
	public function show( $repo_slug, $account_name = null )
	{
		$response = null;

		$this->checkUsername( $account_name );
		Helper::format_slug( $repo_slug );

		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/followers" );

		return $response;
	}

}
