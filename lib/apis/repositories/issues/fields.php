<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace
{
	interface IField {}
	interface IEnum{}

	abstract class FieldBase
	{
		const __default = null;

		public function __toString()
		{
			return (string)self::__default;
		}
	}

	class Status extends FieldBase implements IField, IEnum {
		const __default = self::STATUS_NEW;

		const STATUS_NEW = "new";
		const STATUS_OPEN = "open";
		const STATUS_RESOLVED = "resolved";
		const STATUS_ONHOLD = "on hold";
		const STATUS_INVALID = "invalid";
		const STATUS_DUPLICATE = "duplicate";
		const STATUS_WONTFIX = "wontfix";
	}

	class Priority extends FieldBase implements IField, IEnum {
		const __default = self::PRIORITY_TRIVIAL;

		const PRIORITY_TRIVIAL = "trivial";
		const PRIORITY_MINOR = "minor";
		const PRIORITY_MAJOR = "major";
		const PRIORITY_CRITICAL = "critical";
		const PRIORITY_BLOCKER = "blocker";

	}

	class Kind extends FieldBase implements IField, IEnum{
		const __default = self::KIND_BUG;

		const KIND_BUG = "bug";
		const KIND_ENHANCEMENT = "enhancement";
		const KIND_PROPOSAL = "proposal";
		const KIND_TASK = "task";

	}

	class Title extends FieldBase implements IField {}

	class Responsible extends FieldBase implements IField {}

	class Content extends FieldBase implements IField {}

	class Component extends FieldBase implements IField {}

	class Milestone extends FieldBase implements IField {}

	class Version extends FieldBase implements IField {}
}
/******************************************** Bread And Buttah ********************************************/

namespace bitbucket\api
{
	use \bitbucket\api\repositories\Issues;

	/**
	 *  @TODO Description
	 *
	 * @author Anthony Steiner <asteiner@provisionists.com>
	 * @package Bitbucket Api Library
	 * @method \bitbucket\api\repositories\issues\Fields title() title(string $value) Sets the Title
	 * @method \bitbucket\api\repositories\issues\Fields status() status(Status $value) Sets the status
	 * @method \bitbucket\api\repositories\issues\Fields priority() priority(Priority $value) Sets the Priority
	 * @method \bitbucket\api\repositories\issues\Fields responsible() responsible(string $value) Sets the Responsible
	 * @method \bitbucket\api\repositories\issues\Fields content() content(string $value) Sets the Content
	 * @method \bitbucket\api\repositories\issues\Fields kind() kind(Kind $value) Sets the Kind
	 * @method \bitbucket\api\repositories\issues\Fields component() component(string $value) Sets the Component
	 * @method \bitbucket\api\repositories\issues\Fields milestone() milestone(string $value) Sets the Milestone
	 * @method \bitbucket\api\repositories\issues\Fields version() version(string $value) Sets the Version
	 */
	class IssueFields {

		/**
		 * Referenced repository api object
		 * @var Issues
		 */
		private $_issue_api;

		/**
		 * The base api reference
		 * @var Api
		 */
		private $_api;

		/**
		 * @var Title
		 */
		public $title;

		/**
		 * A string containing the issue title.
		 * @var Status
		 */
		public $status;

		/**
		 * A enumerator representing the issue priority. Valid priority values are
		 * @var Priority
		 */
		public $priority;

		/**
		 * @var Responsible
		 */
		public $responible;

		/**
		 * @var Content
		 */
		public $content;

		/**
		 * @var Kind
		 */
		public $kind;

		/**
		 * @var Component
		 */
		public $component;

		/**
		 * @var Milestone
		 */
		public $milestone;

		/**
		 * @var Version
		 */
		public $version;

		public function __call($method, $parameters)
		{
			if (property_exists($this, $method))
			{
				$value = $parameters[0];
				if (!empty($value))
				{
					$reflectedClass = new \ReflectionClass ("\\".ucfirst($method));
					$class_constants = $reflectedClass->getConstants();

					if ( in_array("\\IEnum", class_implements("\\".ucfirst($method))))
					{
						if (!in_array($value, $class_constants))
						{
							return $this;
						}
					}

					$this->{$method} = $value;
					return $this;
				}
			}
		}

		/**
		 * Force class to return the built filter string when type-boxed with a string scalar
		 */
		public function __toString()
		{
			// Get Class & instantiate
			$refclass = new \ReflectionClass($this);
			$name = "";
			$stringBuilder = array();

			// Loop through the public properties
			foreach ($refclass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property)
			{
				// Substantiate property name to a local
				$name = $property->name;

				if (strtolower($name) == 'title' && (empty($this->{$name}) || strlen($this->{$name}) == 0))
				{
					return "Title field value is required ".TITLE_MISSING;
				}

				if (!empty($this->{$name}) && strlen($this->{$name}) > 0)
				{
					$stringBuilder[] = sprintf('%s=%s',strtolower($name),$this->{$name});
				}
			}

			return implode("&", $stringBuilder);
		}

		/**
		 * @TODO Description
		 * @param unknown_type $issue_api
		 * @param unknown_type $api
		 */
		public function __construct($issue_api = null, $api = null)
		{
			if (!empty($issue_api) || !empty($issue_api) )
			{
				$this->_issue_api =& $issue_api;
				$this->_api = $api;
			}

			// Get Class & instantiate
			$refclass = new \ReflectionClass($this);
			$name = "";

			// Loop through the public properties
			foreach ($refclass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
				// Substantiate property name to a local
				$name = $property->name;

				if ($property->class == $refclass->name && $name != 'stringBuilder') {
					$matches = array();
					preg_match('/@var (?<Class>\w+)/is',
							$property->getDocComment(), $matches);
					$class = $matches["Class"];
					$class = "\\".$class;
					$this->{$property->name} = new $class();
				}
			}
		}

		/**
		 * @TODO Description
		 * @param unknown_type $repo_slug
		 * @param unknown_type $account_name
		 * @return \bitbucket\api\Ambigous
		 */
		public function create($repo_slug, $account_name = null)
		{
			if ($this->_issue_api == null)
			{
				$this->_issue_api = new Issues($this->_api);
			}

			return $this->_issue_api->create($repo_slug, $account_name, $this);
		}
	}
}