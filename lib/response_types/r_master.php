<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 * @todo Design Response Master Class class
 */
namespace bitbucket\api\responses;

/**
 * Seems that this is where most response classes start,
 * figured it would make more sense to have a building block
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 * @subpackage Responses
 */
abstract class r_Master
{

    /**
     * Class Constructor
     * Using reflection, it creates a psuedo-class via the json object
     * @param mixed $object
     */
    public function __construct( $object )
    {
        $refclass = new \ReflectionClass( $this );
        foreach ( $refclass->getProperties( \ReflectionProperty::IS_PUBLIC ) as $property )
        {
            $this->{$property->name} = $object->{$property->name};
        }
    }
}
