<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\users;

use bitbucket\api\ApiBase;
use bitbucket\api\Api;
use bitbucket\api\Helper;
use bitbucket\api\users_privileges;



/**
 * Use this resource to manage privilege settings for a team account.
 * Team accounts can grant groups account privileges as well as repository access.
 * Groups with account privileges are those with can administer this account (admin rights) or can create repositories in this account (collaborator rights) checked.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Privileges extends ApiBase
{

    /**
     * Gets the groups with account privileges defined for a team account. Administer rights automatically include collaborator rights. If you have a group without either option set, it is omitted from the results. The caller must authenticate. The access credentials must come from an account with owner or administrative privileges � either the team account access credentials or a member who can administer the team account.
     * @param string $account_name The team or individual account name.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function show( $account_name = null)
    {
        $response = null;

        $this->checkUsername( $account_name );

        $response = $this->api->get( "/users/{$account_name}/privileges" );

        return $response;
    }

    /**
     * Gets the privilege associated with the specified groupname. The privilege is can be either collaborator or admin. The caller must authenticate. The access credentials must come from an account with owner or administrative privileges � either the team account access credentials or a member who can administer the team account.
     * @param string $group_slug	The group's slug.
     * @param string $account_owner	The account that owns the group.
     * @param string $account_name	The team or individual account name.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function show_group( $group_slug, $account_owner = null, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );
        $this->checkUsername( $account_owner );
        Helper::format_slug( $group_slug );

        $response = $this->api->get( "/users/{$account_name}/privileges/{$account_owner}/{$group_slug}" );

        return $response;
    }

    /**
     * Updates an existing group's privileges for a team account. You can set a group's privileges to admin or collaborator. This call returns the changed group. The caller must authenticate. The access credentials must come from an account with owner or administrative privileges � either the team account access credentials or a member who can administer the team account.
     * @param string $group_slug	The group's slug.
     * @param string $privileges	Either admin or collaborator
     * @param string $account_owner	The account that owns the group.
     * @param string $account_name	The team or individual account name.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function update( $group_slug, $privileges = null, $account_owner = null, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );
        $this->checkUsername( $account_owner );
        Helper::format_slug( $group_slug );
        users_privileges::check( $privileges );

        $response = $this->api->put( "/users/{$account_name}/privileges", array(
            'privileges' => $privileges
        ) );

        return $response;
    }

    /**
     * Adds a privilege to a group without any. The caller must authenticate. The access credentials must come from an account with owner or administrative privileges � either the team account access credentials or a member who can administer the team account.
     * @param string $group_slug	The group's slug.
     * @param string $privileges	Either admin or collaborator.
     * @param string $account_owner	The account that owns the group.
     * @param string $account_name	The team or individual account name.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function create( $group_slug, $privileges = null, $account_owner = null, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );
        $this->checkUsername( $account_owner );
        Helper::format_slug( $group_slug );
        users_privileges::check( $privileges );

        $response = $this->api->post(  "/users/{$account_name}/privileges/{$account_owner}/{$group_slug}", array(
            'privileges' => $privileges
        ) );

        return $response;
    }

    /**
     * Deletes a privilege. The caller must authenticate. The access credentials must come from an account with owner or administrative privileges � either the team account access credentials or a member who can administer the team account.
     * @param string $group_slug	The group's slug.
     * @param string $account_owner	The account that owns the group.
     * @param string $account_name	The team or individual account name.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function delete( $group_slug, $account_owner = null, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $response = $this->api->get( "/users/{$account_name}/privileges/{$account_owner}/{$group_slug}");

        return $response;
    }
}