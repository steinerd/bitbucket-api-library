<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\users;

use bitbucket\api\Api;
use bitbucket\api\ApiBase;
use bitbucket\api\Helper;

/**
 * An invitation is a request sent to an external email address to participate one or more of an account's groups. Any user with admin access to the account can invite someone to a group.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Invitations extends ApiBase
{

    /**
     * Gets a list of pending invitations on a team or individual account. This call requires authorization and the caller must have administrative rights on the account.
     * @param string $account_name	The name of an individual or team account.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function show( $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $response = $this->api->get( "/users/{$account_name}/invitations" );

        return $response;
    }

    /**
     * Gets any pending invitations on a team or individual account for a particular email address. Any user with admin access to the account can invite someone to a group. This call requires authorization and the caller must have administrative rights on the account.
     * @param string $email_address	The email address to get.
     * @param string $account_name	The name of an individual or team account.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function by_email( $email_address, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $response = $this->api->get( "/users/{$account_name}/invitations/{$email_address}" );

        return $response;
    }

    /**
     * Tests whether there is a pending invitation for a particular email on account's group. An invitation is a request sent to an external email address to participate one or more of an account's groups. Any user with admin access to the account can invite someone to a group. This call requires authorization and the caller must have administrative rights on the account. T
     * @param string $email_address	Name of the email address to delete.
     * @param string $group_owner	The name of an individual or team account that owns the group.
     * @param string $group_slug	An identifier for the group. The slug is an identifier constructed by the Bitbucket service. Bitbucket creates a slug by converting spaces to dashes and making all text lower case.
     * @param string $account_name	The name of an individual or team account
     * @return boolean
     */
    public function group( $email_address, $group_owner, $group_slug, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );
        Helper::format_slug($group_slug);

        $this->api->get( "/users/{$account_name}/invitations/{$email_address}/{$group_owner}/{$group_slug}" );

        $response = $this->api->getRequest()->http_code == '200' ? true : false;

        return $response;
    }

    /**
     * Issues an invitation to the specified account group. An invitation is a request sent to an external email address to participate one or more of an account's groups. Any user with admin access to the account can invite someone to a group. This call requires authorization and the caller must have administrative rights on the account. This call does not check validate the email address.
     *
     * @param string $email_address	Name of the email address to delete.
     * @param string $group_owner	The name of an individual or team account that owns the group.
     * @param string $group_slug	An identifier for the group. The slug is an identifier constructed by the Bitbucket service. Bitbucket creates a slug by converting spaces to dashes and making all text lower case.
     * @param string $account_name	The name of an individual or team account
     * @return boolean
     */
    public function send( $email_address, $group_owner, $group_slug, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );
        Helper::format_slug($group_slug);

        // The email in the post data is actually just a content buffer, server refuses PUT request without Content-Length: 0
        $this->api->put( "/users/{$account_name}/invitations/{$email_address}/{$group_owner}/{$group_slug}", array("email" => $email_address));
        $response = $this->api->getRequest()->http_code == '200' ? true : false;

        return $response;
    }

    /**
     * Deletes any pending invitations on a team or individual account for a particular email address. An invitation is a request sent to an external email address to participate one or more of an account's groups. If a email is invited on multiple groups, the invitation is removed from all groups. This call requires authorization and the caller must have administrative rights on the account.
     * @param string $email_address	The email address to get.
     * @param string $account_name	The name of an individual or team account.
     * @return boolean
     */
    public function delete( $email_address, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $this->api->delete( "/users/{$account_name}/invitations/{$email_address}" );
        $response = $this->api->getRequest()->http_code == '204' ? true : false;

        return $response;
    }

    /**
     * Deletes a pending invitation for a particular email on account's group. An invitation is a request sent to an external email address to participate one or more of an account's groups. Any user with admin access to the account can invite someone to a group. This call requires authorization and the caller must have administrative rights on the account.
     * @param string $email_address	Name of the email address to delete.
     * @param string $group_owner	The name of an individual or team account that owns the group.
     * @param string $group_slug	An identifier for the group. The slug is an identifier constructed by the Bitbucket service. Bitbucket creates a slug by converting spaces to dashes and making all text lower case.
     * @param string $account_name	The name of an individual or team account
     * @return boolean
     */
    public function delete_by_group( $email_address, $group_owner, $group_slug, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );
        Helper::format_slug($group_slug);

        $this->api->delete( "/users/{$account_name}/invitations/{$email_address}/{$group_owner}/{$group_slug}" );
        $response = $this->api->getRequest()->http_code == '204' ? true : false;

        return $response;
    }
}