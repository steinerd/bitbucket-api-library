<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 */
namespace bitbucket\api;

require_once 'lib/api.php';

/**
 * The invitations endpoint allows repository administrators to send email invitations
 * 	to grant read, write, or admin privileges to a repository.
 * 	The email sent to a user includes a URL for accepting the invitation.
 *  If the recipient already has a Bitbucket account corresponding to that email address,
 *  he or she must log into that account to access the repository.
 *  If the user does not have a Bitbucket account, the user must create a Bitbucket account before accessing the repository.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Invitations extends ApiBase
{
    /**
     * Invites a user to a repository.
     *
     * @param string	$to_address		The email recipient.
     * @param string	$repo_slug		A repository belonging to the account.
     * @param string	$permission		The permission the recipient is granted.
     * @param string	$account_name	The team or individual account.
     * @return Request
     */
    public function send($to_address, $repo_slug, $permission, $account_name = null)
    {
        $response = null;
        $this->checkUsername($account_name);

        Helper::format_slug($repo_slug);
        permission_enum::check($permission);

        $response = $this->api->post("invitations/$account_name/$repo_slug/$to_address", array("permission" => $permission));

        return $response;
    }
}