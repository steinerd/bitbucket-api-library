<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\users;

use \bitbucket\api\Api;
use \bitbucket\api\ApiBase;

/**
 * This resource returns a user structure and the repositories array associated with an existing account.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Account extends ApiBase
{

    /**
     * Gets the information associated with an account.
     * The information returned by this call depends on whether you
     * authenticate with the accountname:password combination or not.
     * An an unauthenticated gets a user's basic information and lists any public repositories for that user.
     *
     * @param	string	$account_name 	The name of an individual or team account ACCOUNTS ARE CASE SENSITIVE! You can also use a validated email address in place of the accountname value.
     */
    public function profile( $account_name = null )
    {
        $response = null;

        $this->checkUsername($account_name);

        $response = $this->api->get( "/users/{$account_name}" );

        return $response;
    }

    /**
     * Gets the number of users counted against an account's plan.
     *
     * @param	string	$account_name 	The name of an individual or team account. You can also use a validated email address in place of the accountname value.
     */
    public function plan( $account_name = null  )
    {
        $response = null;

        $this->checkUsername($account_name);

        $response = $this->api->get( "/users/{$account_name}/plan" );

        return $response;
    }

    /**
     * Gets a count and the list of accounts following an account.
     * Use this API to get a list of the individuals following an account.
     * Currently, the Bitbucket UI does not list each account, it only displays the count.
     *
     * @param	string	$account_name 	The name of an individual or team account. You can also use a validated email address in place of the accountname value.
     */
    public function followers( $account_name = null  )
    {
        $response = null;

        $this->checkUsername($account_name);

        $response = $this->api->get( "/users/{$account_name}/followers" );

        return $response;
    }
}

