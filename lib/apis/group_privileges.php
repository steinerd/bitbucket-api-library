<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 */
namespace bitbucket\api;

require_once 'lib/constants.php';

/**
 * Use the group-privileges resource to query and manipulate the group privileges (permissions) of an account's repositories.
 * An account owner (or team account administrator) defines groups at the account level.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 * @subpackage API.Group Privileges
 */
class GroupPrivileges extends ApiBase
{
    /**
     * Gets an array of all the groups granted access to an account's repositories.
     * The caller must authenticate as a user with administrative rights on the account.
     *
     * @param	string	$account_name	The team or individual account.
     * @return	Ambigous	<\bitbucket\api\Ambigous, object, mixed>
     */
    public function show($account_name = null)
    {
        $username = null;
        $response = null;

        $this->checkUsername($username);
        $this->checkUsername($account_name);

        $response = $this->api->get("group-privileges/$account_name");

        return $response;
    }

    /**
     * GET a list of the privilege groups for a specific repository.
     * The caller must authenticate successfully and have administrative rights on the account.
     *
     * @param	string	$repo_slug		A repository belonging to the account.
     * @param	string	$account_name	(Optional) The team or individual account. Omit to use the authenticated user.
     * @return	Ambigous	<\bitbucket\api\Ambigous, object, mixed>
     */
    public function repository($repo_slug, $account_name = null)
    {
        $username = null;
        $response = null;

        $this->checkUsername($username);
        $this->checkUsername($account_name);

        Helper::format_slug($repo_slug);

        $response = $this->api->get("group-privileges/$account_name/$repo_slug");

        return $response;
    }


    /**
     * Get a list of the repositories on which a particular privilege group appears.
     * This method operates on a single account, it does not list across acounts.
	 * The caller must authenticate as a user with administrative rights on the account.
	 *
     * @param	string			$group_slug		The group slug. The slug is an identifier constructed by the Bitbucket service.
     * @param	string			$group_owner	(Optional) The team or individual account.  Omit to use the authenticated user.
     * @param	string			$account_name	(Optional) The account that owns the group.  Omit to use the authenticated user.
     * @return	Ambigous		<\bitbucket\api\Ambigous, object, mixed>
     */
    public function repositories($group_slug, $group_owner = null, $account_name = null)
    {
        $username = null;
        $response = null;

        $this->checkUsername($username);
        $this->checkUsername($group_owner);
        $this->checkUsername($account_name);

        Helper::format_slug($group_slug);

        $response = $this->api->get("group-privileges/$account_name/$group_owner/$group_slug");

        return $response;
    }

    /**
     * Gets the privileges of a group on a repository.
     * The caller must authenticate as a user with administrative rights on the account.
     *
	 * @param	string			$repo_slug		A repository belonging to the account.
	 * @param	string			$group_slug		The group slug. The slug is an identifier constructed by the Bitbucket service.
     * @param	string			$group_owner	(Optional) The account that owns the group.  Omit to use the authenticated user.
	 * @param	string			$account_name	(Optional) The team or individual account.  Omit to use the authenticated user.
     * @return	Ambigous		<\bitbucket\api\Ambigous, object, mixed>
     */
    public function group($repo_slug, $group_slug, $group_owner = null, $account_name = null)
    {
        $username = null;
        $response = null;

        $this->checkUsername($username);
        $this->checkUsername($group_owner);
        $this->checkUsername($account_name);

        Helper::format_slug($repo_slug);
        Helper::format_slug($group_slug);

        $response = $this->api->get("group-privileges/$account_name/$repo_slug/$group_owner/$group_slug");

        return $response;
    }

    /**
     * GRANT group privileges on a repository.  The caller must authenticate as a user with administrative rights on the account.
     *
     * @param	string					$repo_slug		The repository to grant privileges on.
     * @param	string					$group_slug		The group slug. The slug is an identifier constructed by the Bitbucket service.
     * @param	privilege_enum|string	$privilege		A privilege value. Use @see privilege_enum
     * @param	string					$group_owner	(Optional) The account that owns the group.  Omit to use the authenticated user.
     * @param	string					$account_name	(Optional) The team or individual account.  Omit to use the authenticated user.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function grant($repo_slug, $group_slug, $privilege, $group_owner = null, $account_name = null)
    {
        $username = null;
        $response = null;

        $this->checkUsername($username);
        $this->checkUsername($group_owner);
        $this->checkUsername($account_name);

        Helper::format_slug($repo_slug);
        Helper::format_slug($group_slug);
        privilege_enum::check($privilege);

        $response = $this->api->put("group-privileges/$account_name/$repo_slug/$group_owner/$group_slug", $privilege);

        return $response;
    }

    /**
     * DELETE a privilege group from a repository.  The caller must authenticate as a user with administrative rights on the account.
     *
     * @param	string		$repo_slug		The repository to grant privileges on.
     * @param	string		$group_slug		The group slug. The slug is an identifier constructed by the Bitbucket service.
     * @param	string		$group_owner	(Optional) The account that owns the group.  Omit to use the authenticated user.
     * @param	string		$account_name	(Optional) The team or individual account.  Omit to use the authenticated user.
     * @return boolean
     */
    public function revoke_repository($repo_slug, $group_slug, $group_owner = null, $account_name = null)
    {
        $username = null;
        $response = null;

        $this->checkUsername($username);
        $this->checkUsername($group_owner);
        $this->checkUsername($account_name);

        Helper::format_slug($repo_slug);
        Helper::format_slug($group_slug);

        $this->api->delete("group-privileges/$account_name/$repo_slug/$group_owner/$group_slug");

        $response = $this->api->getRequest()->http_code == "204" ? true : false;

        return (bool)$response;
    }

    /**
     * Deletes the privileges for a group on every repository where it appears.
     * The caller must authenticate as a user with administrative rights on the account.
     *
     * @param	string		$group_slug		The group slug. The slug is an identifier constructed by the Bitbucket service.
     * @param	string		$group_owner	(Optional) The account that owns the group.  Omit to use the authenticated user.
     * @param	string		$account_name	(Optional) The team or individual account.  Omit to use the authenticated user.
     * @return boolean
     */
    public function revoke($group_slug, $group_owner = null, $account_name = null)
    {
        $username = null;
        $response = null;

        $this->checkUsername($username);
        $this->checkUsername($group_owner);
        $this->checkUsername($account_name);

        Helper::format_slug($group_slug);

        $this->api->delete("group-privileges/$account_name/$group_owner/$group_slug");

        $response = $this->api->getRequest()->http_code == "204" ? true : false;

        return (bool)$response;
    }


}


