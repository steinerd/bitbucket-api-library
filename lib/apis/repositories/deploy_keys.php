<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\repositories;

use \bitbucket\api\Helper;
use \bitbucket\api\Api;
use \bitbucket\api\ApiBase;


/**
 * Manage ssh keys used for deploying product builds. All the calls for this resource require authentication as the account owner.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class DeployKeys extends ApiBase
{
	/**
	 * Gets a list of the keys associated with an repository.
	 * @param string $repo_slug		The repo identifier.
	 * @param string $account_name	The team or indivisual account.
	 * @return \bitbucket\api\Ambigous
	 */
	public function show($repo_slug, $account_name = null)
	{
		$response = null;

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/deploy-keys");

		return $response;
	}

	/**
	 * Gets the content of the specified key_id. This call requires authentication.
	 * @param string	$repo_slug		The repo identifier.
	 * @param int		$key_id			The key identifier.
	 * @param string	$account_name	The team or indivisual account.
	 * @return \bitbucket\api\Ambigous
	 */
	public function key($repo_slug, $key_id, $account_name = null)
	{
		$response = null;

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/deploy-keys/{$key_id}");

		return $response;
	}

	/**
	 * Creates a key on the specified account. You must supply a valid key that is unique across the Bitbucket service.
	 * @param string $repo_slug		The repo identifier.
	 * @param string $key				The content of the key.
	 * @param string $label			A display name for the key.
	 * @param string $account_name	The team or indivisual account.
	 * @return \bitbucket\api\Ambigous
	 */
	public function create($repo_slug, $key, $label = "", $account_name = null)
	{
		$response = null;
		$data = array();

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		$data = array(
					'key' => $key,
					'label' => $label
				);
		$data = array_filter($data);

		$response = $this->api->post( "/repositories/{$account_name}/{$repo_slug}/deploy-keys", $data);

		return $response;
	}

	/**
	 * Updates a key on the specified account. You must supply a valid key_id (pk field)  that is unique across the Bitbucket service. You cannot update the key_id itself.
	 * @param string $repo_slug		The repo identifier.
 	 * @param int $key_id			The key identifier.
	 * @param string $key			The content of the key.
	 * @param string $label			A display name for the key.
	 * @param string $account_name	The team or indivisual account.
	 * @return \bitbucket\api\Ambigous
	 */
	public function update($repo_slug, $key_id, $key, $label = "", $account_name = null)
	{
		$response = null;
		$data = array();

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		$data = array(
				'key' => $key,
				'label' => $label
		);
		$data = array_filter($data);

		$response = $this->api->put( "/repositories/{$account_name}/{$repo_slug}/deploy-keys/{$key_id}", $data);

		return $response;
	}

	/**
	 * Deletes the key specified by the key_id value. This call requires authentication.
	 * @param string	$repo_slug		The repo identifier.
	 * @param int		$key_id			The key identifier.
	 * @param string	$account_name	The team or indivisual account.
	 * @return boolean
	 */
	public function delete($repo_slug, $key_id, $account_name = null)
	{
		$response = null;

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		$this->api->delete( "/repositories/{$account_name}/{$repo_slug}/deploy-keys/{$key_id}");
		$response = $this->api->getRequest()->http_code == '204' ? true : false;

		return $response;
	}
}