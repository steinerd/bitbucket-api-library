<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\repositories;

use \bitbucket\api\Helper;
use \bitbucket\api\Api;
use \bitbucket\api\ApiBase;
use \bitbucket\api\enum_functions;


/**
 * You can use events to track events that occur on public repositories. Currently, Bitbucket does not support displaying events from private repositories. This endpoint does not require authentication and is a read-only resource.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Events extends ApiBase
{
	/**
	 * @var Event_Types_Emum Substantiated enumeration of available event types
	 */
	public $types;

	/**
	 * Default constructor, substantiates types with custom class-based enum-type
	 * @param Api $api Default API instance
	 */
	public function __construct($api)
	{
		parent::__construct($api);
		$this->types = New Event_Types_Emum();
	}


	/**
	 * Gets a list of a repository's events associated with the specified repo_slug. By default, this call returns the top 50 events.
	 * @param string $repo_slug				The repo identifier.
	 * @param Event_Types_Emum|string $type	Use {@see \bitbucket\api\repositories\Event_Types_Emum} for the best resutls. The event type to return. If you specify a type parameter, the count contains the total number of events of that type associated with the account.
	 * @param int $start					An integer specifying the offset to start with. By default, this call starts with 0.
	 * @param int $limit					An integer specifying the number of events to return. You can specify a value between 0 and 50. If you specify 0, the system returns the count but the events array is empty.
	 * @param string $account_name			The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function show($repo_slug, $type = Event_Types_Emum::NOTHING, $start = 0, $limit = 50, $account_name = null)
	{
		$response = null;
		$data = array();
		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);
		$this->types->check($type);

		if ($limit < 0)
		{
			$limit = 0;
		}
		elseif($limit > 50)
		{
			$limit = 50;
		}
		else
		{
			$limit = 50;
		}

		if (! $start > 0)
		{
			$start = 0;
		}

		if ($type == Event_Types_Emum::NOTHING)
		{
			$type = null;
		}

		$data = array('limit' => $limit, 'start' => $start, 'type' => $type);
		$data = array_filter($data);

		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/events", $data);

		return $response;
	}
}

/**
 * Custom Enumeration class for simpler type filtering
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Event_Types_Emum extends enum_functions
{
	/**
	 * @var string Default value, nothing, null; no filter
	 */
	const NOTHING = '--nothing--';

	/**
	 * @var string User committed to the repository.
	 */
	const COMMIT = 'commit';

	/**
	 * @var string User created a repository.
	 */
	const CREATE = 'create';

	/**
	 * @var string User created a change set comment.
	 */
	const CSET_COMMENT_CREATED = 'cset_comment_created';

	/**
	 * @var string User deleted a change set comment.
	 */
	const CSET_COMMENT_DELETED = 'cset_comment_deleted';

	/**
	 * @var string User updated a change set comment.
	 */
	const CSET_COMMENT_UPDATED = 'cset_comment_updated';

	/**
	 * @var string User approved a change set.
	 */
	const CSET_LIKE = 'cset_like';

	/**
	 * @var string User removed approval from a change set.
	 */
	const CSET_UNLIKE = 'cset_unlike';

	/**
	 * @var string User deleted a repository.
	 */
	const DELETE = 'delete';

	/**
	 * @var string User uploaded a file � for example an icon.
	 */
	const FILE_UPLOADED = 'file_uploaded';

	/**
	 * @var string User �forked the repository.
	 */
	const FORK = 'fork';

	/**
	 * @var string User updated an issue comment.
	 */
	const ISSUE_COMMENT = 'issue_comment';

	/**
	 * @var string User updated an issue.
	 */
	const ISSUE_UPDATE = 'issue_update';

	/**
	 * @var string User created a patch queue for the repository. This is only valid with Mercurial repositories.
	 */
	const MQ = 'mq';

	/**
	 * @var string User created a comment on a pull request.
	 */
	const PULLREQUEST_COMMENT_CREATED = 'pullrequest_comment_created';

	/**
	 * @var string User deleted a comment on a pull request.
	 */
	const PULLREQUEST_COMMENT_DELETED = 'pullrequest_comment_deleted';

	/**
	 * @var string User updated a comment on a pull request.
	 */
	const PULLREQUEST_COMMENT_UPDATED = 'pullrequest_comment_updated';

	/**
	 * @var string User created a pull request.
	 */
	const PULLREQUEST_CREATED = 'pullrequest_created';

	/**
	 * @var string User accepted a pull request.
	 */
	const PULLREQUEST_FULFILLED = 'pullrequest_fulfilled';

	/**
	 * @var string User approved a pull request.
	 */
	const PULLREQUEST_LIKE = 'pullrequest_like';

	/**
	 * @var string User rejected a pull request.
	 */
	const PULLREQUEST_REJECTED = 'pullrequest_rejected';

	/**
	 * @var string User marked a pull request as superseded.
	 */
	const PULLREQUEST_SUPERSEDED = 'pullrequest_superseded';

	/**
	 * @var string User removed approval from a pull request.
	 */
	const PULLREQUEST_UNLIKE = 'pullrequest_unlike';

	/**
	 * @var string User updated a pull request.
	 */
	const PULLREQUEST_UPDATED = 'pullrequest_updated';

	/**
	 * @var string User reported an issue.
	 */
	const REPORT_ISSUE = 'report_issue';

	/**
	 * @var string User started following an issue.
	 */
	const START_FOLLOW_ISSUE = 'start_follow_issue';

	/**
	 * @var string User started following a repository.
	 */
	const START_FOLLOW_REPO = 'start_follow_repo';

	/**
	 * @var string User started following an issue.
	 */
	const START_FOLLOW_USER = 'start_follow_user';

	/**
	 * @var string User stopped following an issue.
	 */
	const STOP_FOLLOW_ISSUE = 'stop_follow_issue';

	/**
	 * @var string User stopped following a repository.
	 */
	const STOP_FOLLOW_REPO = 'stop_follow_repo';

	/**
	 * @var string User stopped following a user.
	 */
	const STOP_FOLLOW_USER = 'stop_follow_user';

	/**
	 * @var string User stripped a repository.
	 */
	const STRIP = 'strip';

	/**
	 * @var string The user changed the administrative access on a team.
	 */
	const TEAM_ACCESS_CHANGED_ADMIN = 'team_access_changed_admin';

	/**
	 * @var string The user changed the collaborator acces on a team.
	 */
	const TEAM_ACCESS_CHANGED_COLLAB = 'team_access_changed_collab';

	/**
	 * @var string The user was granted administrative rights on a team account.
	 */
	const TEAM_ACCESS_GAINED_ADMIN = 'team_access_gained_admin';

	/**
	 * @var string A user was granted collaboration rights on a team account.
	 */
	const TEAM_ACCESS_GAINED_COLLAB = 'team_access_gained_collab';

	/**
	 * @var string A user lost access to a team account.
	 */
	const TEAM_LOST_ACCESS = 'team_lost_access';

	/**
	 * @var string User added a wiki page.
	 */
	const WIKI_CREATED = 'wiki_created';

	/**
	 * @var string User updated a wiki page.
	 */
	const WIKI_UPDATED = 'wiki_updated';


	/**
	 * Default constrcutor
	 */
	public function __construct()
	{
		$acceptable_values = array
					(
						"commit",
						"create",
						"cset_comment_created",
						"cset_comment_deleted",
						"cset_comment_updated",
						"cset_like",
						"cset_unlike",
						"delete",
						"file_uploaded",
						"fork",
						"issue_comment",
						"issue_update",
						"mq",
						"pullrequest_comment_created",
						"pullrequest_comment_deleted",
						"pullrequest_comment_updated",
						"pullrequest_created",
						"pullrequest_fulfilled",
						"pullrequest_like",
						"pullrequest_rejected",
						"pullrequest_superseded",
						"pullrequest_unlike",
						"pullrequest_updated",
						"report_issue",
						"start_follow_issue",
						"start_follow_repo",
						"start_follow_user",
						"stop_follow_issue",
						"stop_follow_repo",
						"stop_follow_user",
						"strip",
						"team_access_changed_admin",
						"team_access_changed_collab",
						"team_access_gained_admin",
						"team_access_gained_collab",
						"team_lost_access",
						"wiki_created",
						"wiki_updated",
						"--nothing--"
					);
		parent::__construct($acceptable_values,"--nothing--");
	}
}
