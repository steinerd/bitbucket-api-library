<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 * @todo Design Group Privileges Response Class class
 */
namespace bitbucket\api\responses;

require 'r_master.php';

/**
 * Future site of Group Privileges Response Class
 * @since v%{major}.%{minor}.5-rc
 *
 */
final class Response_GroupPrivileges extends r_Master
{

    /**
     * Class Constructor
     * @param unknown_type $object
     */
    public function __construct($object)
    {
        parent::__construct($object);
    }
}