<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 */
namespace bitbucket\api;

/**
 * Statc Helper class
 */
class Helper
{
    /**
     * This helper function will reduce a normal string into a valid
     * bitbucket slug.
     *
     * @param string $slug Slug to format
     * @return string formatted slug
     */
    static public function format_slug( &$slug )
    {
    	$slug = preg_replace('/[^-\.\s\dA-z]/i', "", $slug);
    	$slug = preg_replace('/\s/i', "-", $slug);
    	$slug = strtolower($slug);
        return $slug;
    }
}


/**
 * Gets the current instance of the instantiated API class byreference
 * @return \bitbucket\api\Api
 */
function &get_instance()
{
	return Api::get_instance();
}